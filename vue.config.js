module.exports = {
  publicPath: process.env.VUE_APP_PUBLIC_PATH,
  // indexPath:
  devServer: {
    public: '127.0.0.1:8080', // this removes the sockjs-node issue
    disableHostCheck: true, // this removes invalid-host-header when starting the app
    port: process.env.PORT || 8080,
    open: true,
    index: process.env.VUE_APP_PROD_INDEX,
    compress: true,
    contentBase: [('dist/css'), ('dist/img'), ('dist/img')],
    proxy: {
      '/api': {
        target: process.env.VUE_APP_API_TARGET,
        changeOrigin: true,
        secure: false
      }
    },
    overlay: {
      warnings: true,
      errors: true
    }
  },
  css: {
    // CSS Sourcemaps off by default because relative paths are "buggy"
    // with this option, according to the CSS-Loader README
    // (https://github.com/webpack/css-loader#sourcemaps)
    // In our experience, they generally work as expected,
    // just be aware of this issue when enabling this option.
    sourceMap: false
  }
};
