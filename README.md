[![pipeline status](https://gitlab.com/lintol/lintol-frontend/badges/master/pipeline.svg)](https://gitlab.com/lintol/lintol-frontend/commits/master)
[![coverage report](https://gitlab.com/lintol/lintol-frontend/badges/master/coverage.svg?job=test)](https://lintol.gitlab.io/lintol-frontend/)

# lintol-frontend

For more details, see [https://lintol.io/](https://lintol.io/).

> Front end project for lintol application

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Run your unit tests
```
npm run test:unit
```

# To have a mock setup. These commands will start the json mock server.
git clone https://github.com/lintol/jsonapi-server-mock-lintol.git
cd jsonapi-server-mock-lintol
npm start

# To start the front in mock mode to connect the jsonapi server
npm run mock

```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).