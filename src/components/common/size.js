export function convertSize (sizeInBytes) {
  if (!sizeInBytes && sizeInBytes !== 0) {
    return sizeInBytes;
  }

  var suffix = 'B';
  var tdp;

  if (sizeInBytes > 1e9) {
    suffix = 'GB';
    tdp = Math.round(sizeInBytes / 1e7) / 100;
  } else if (sizeInBytes > 1e6) {
    suffix = 'MB';
    tdp = Math.round(sizeInBytes / 1e4) / 100;
  } else if (sizeInBytes > 1e3) {
    suffix = 'kB';
    tdp = Math.round(sizeInBytes / 10) / 100;
  } else {
    tdp = sizeInBytes;
  }

  return tdp + suffix;
}
