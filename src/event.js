import Vue from 'vue';

var bus = new Vue({});

class Broadcast {
  constructor (bus) {
    this.bus = bus;
  }

  setStore (store) {
    this.store = store;
  }

  setEcho (echo) {
    this.echo = echo;
    this.initializeHandlers();
  }

  initializeHandlers () {
    if (!this.echo || !this.bus) {
      console.error('Handlers not initialized, no echo or bus.');

      return null;
    }

    this.bus.$on('data-resource-used', (resourceId) => {
      if (!(resourceId in this.store.state.resources.dataResourcesTracked)) {
        console.log('Tracking: ', resourceId);
        this.echo.private('data-resource.' + resourceId).listen(
          '.Lintol\\Capstone\\Events\\DataResourceStatusChangedEvent',
          (payload) => this.store.dispatch('UPDATE_DATA_RESOURCE_STATUS', payload)
        );

        this.echo.private('data-resource.' + resourceId).listen(
          '.Lintol\\Capstone\\Events\\ResultRetrievedEvent',
          (payload) => {
            if (payload.reportJson) {
              this.store.commit('APPEND_REPORTS', [payload.reportJson]);
            }
          }
        );

        this.store.dispatch('RECORD_DATA_RESOURCE_TRACKED', resourceId);
      }
    });
  }
};

var broadcast = new Broadcast(bus);

export { bus, broadcast };
export default bus;
