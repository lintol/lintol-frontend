import Vue from 'vue';
import Router from 'vue-router';
import Application from '@/components/Application';
import Profiles from '@/views/Profiles';
import ProfileTable from '@/components/profiles/ProfileTable';
import Dashboard from '@/components/Dashboard';
import DataResourceDashboard from '@/components/dashboard/DataResourceDashboard';
import EntityDashboard from '@/components/dashboard/EntityDashboard';
import ManageProfile from '@/components/profiles/ManageProfile';
import Processors from '@/views/Processors';
import ProcessorTable from '@/components/processors/ProcessorTable';
import Reports from '@/views/Reports';
import ReportTable from '@/components/reports/ReportTable';
import ReportView from '@/components/reports/reportview/ReportView';
import MapDetails from '@/components/reports/reportview/map/MapDetails';
import ListReportView from '@/components/reports/reportview/ListReportView';
import TabularDetails from '@/components/reports/reportview/tabular/TabularDetails';
import DocumentDetails from '@/components/reports/reportview/document/DocumentDetails';
import ExceptionDetails from '@/components/reports/reportview/exception/ExceptionDetails';
import Resources from '@/views/Resources';
import ResourceTable from '@/components/resources/ResourceTable';
import AddResource from '@/components/resources/AddResource';
import Users from '@/views/Users';
import UserTable from '@/components/users/UserTable';
import MySettings from '@/components/MySettings';

Vue.use(Router);

const routes = [
  {
    path: '/',
    redirect: '/application/profiles/profiletable'
  },
  {
    path: '/application',
    name: 'Application',
    component: Application,
    children: [
      { name: 'dashboard',
        path: 'dashboard',
        component: Dashboard,
        children: [
          { name: 'dataResourceDashboard', path: 'dataresource', component: DataResourceDashboard },
          { name: 'entityDashboard', path: 'entity', component: EntityDashboard }
        ]
      },
      { name: 'profiles',
        path: 'profiles',
        component: Profiles,
        children: [
          { name: 'profileTable', path: 'profiletable', component: ProfileTable },
          { name: 'addProfile', path: 'addprofile', component: ManageProfile, props: true },
          { name: 'editProfile', path: 'editprofile/:profileId', component: ManageProfile, props: true }
        ]
      },
      { name: 'processors',
        path: 'processors',
        component: Processors,
        children: [
          { name: 'processorTable', path: 'processortable', component: ProcessorTable }
        ]
      },
      { name: 'reports',
        path: 'reports',
        component: Reports,
        children: [
          { name: 'reportTable', path: 'reporttable/:entity?/:id?', props: true, component: ReportTable },
          { name: 'reportView',
            path: 'reportview/:reportId',
            component: ReportView,
            props: true,
            children: [
              { name: 'exceptionDetails', path: 'exceptiondetails', component: ExceptionDetails, props: true },
              { name: 'tabularDetails', path: 'tabulardetails', component: TabularDetails, props: true },
              { name: 'documentDetails', path: 'documentdetails', component: DocumentDetails, props: true },
              { name: 'listReportView', path: 'listreportview', component: ListReportView, props: true },
              { name: 'mapDetails', path: 'mapdetails', component: MapDetails, props: true }
            ]
          }
        ]
      },
      { name: 'mySettings',
        path: 'mysettings',
        component: MySettings
      },
      { name: 'resources',
        path: 'resources',
        component: Resources,
        children: [
          { name: 'resourceTable', path: 'resourcetable', component: ResourceTable },
          { name: 'addResource', path: 'addresource', component: AddResource }
        ]
      },
      { name: 'users',
        path: 'users',
        component: Users,
        children: [
          { name: 'userTable', path: 'usertable', component: UserTable }
        ]
      }
    ]
  }
];

/* {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import( webpackChunkName: "about"  '../views/About.vue')
  } */

const router = new Router({
  mode: 'history',
  routes
});

export default router;
