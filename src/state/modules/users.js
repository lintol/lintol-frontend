import { fromState } from '../jsonapi';
import * as a from '../action-types';
import * as m from '../mutation-types';
import axios from 'axios';

var apiPrefix = process.env.VUE_APP_API_PREFIX;

const state = {
  repository: {
    users: {}
  },
  features: [],
  loggedInUser: null,
  currentUser: null
};

// getters
const getters = {
  users: state => {
    return fromState(state).findAll('users');
  },
  featureOn: state => {
    return (feature) => state.features.indexOf(feature) !== -1;
  }
};

// actions
const actions = {
  [a.LOAD_USERS] ({ commit }) {
    return axios.get(apiPrefix + '/users/').then((response) => {
      var users = response.data;
      commit(m.SET_USERS, users);
    }, error => {
      console.log('Couldnt get users.:' + error);
    });
  },
  [a.LOAD_USER] ({ commit }, userId) {
    return axios.get(apiPrefix + '/users/' + userId).then((response) => {
      var user = response.data;
      commit(m.SET_CURRENT_USER, user);
    }, error => {
      console.log('Couldnt get user.:' + error);
    });
  },
  [a.LOAD_LOGGED_IN_USER] ({ commit, state }) {
    if (!state.loggedInUser) {
      return axios.get(apiPrefix + '/users/me').then((response) => {
        var user = response.data;
        commit(m.SET_LOGGED_IN_USER, user);
      }, error => {
        console.log('Couldnt get logged in user:' + error);
      });
    }
  }
};

// mutations
const mutations = {
  [m.SET_USERS] (state, users) {
    state.repository.users = {};
    fromState(state).sync(users);
  },
  [m.RESET_USERS] (state) {
    state.repository.users = {};
  },
  [m.SET_CURRENT_USER] (state, user) {
    var store = fromState(state);
    store.sync(user);
    state.currentUser = store.find('users', user.data.id);
  },
  [m.UNSET_CURRENT_USER] (state) {
    state.currentUser = null;
  },
  [m.SET_LOGGED_IN_USER] (state, user) {
    var store = fromState(state);
    store.sync(user);
    state.loggedInUser = store.find('users', user.data.id);
    state.features = user.data.attributes.featureFlags;
  },
  [m.UNSET_LOGGED_IN_USER] (state) {
    state.loggedInUser = null;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
