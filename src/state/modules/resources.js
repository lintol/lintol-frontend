import { fromState, toModel } from '../jsonapi';
import * as a from '../action-types';
import * as m from '../mutation-types';
import axios from 'axios';
import Vue from 'vue';
import bus from '../../event.js';

var apiPrefix = process.env.VUE_APP_API_PREFIX;

const ACTION_NONE = 'action:none';
const ACTION_PENDING = 'action:pending';
const ACTION_REQUESTED = 'action:requested';

const ResourceMixin = {
  getInfoLine () {
    let text = this.status;
    let code = this.status;
    if (this.lastRunStatus) {
      if (this.lastRunStatus === 'STATUS_RUNNING') {
        code = 'running';
      } else if (this.lastRunStatus === 'STATUS_SUCCEEDED') {
        code = 'report run';
      } else if (this.lastRunStatus === 'STATUS_FAILED') {
        code = 'failed';
      }
    }
    text = code;
    if (this.runsByCompletionStatusCount && typeof this.runsByCompletionStatusCount === 'object') {
      if (this.runsByCompletionStatusCount.STATUS_RUNNING && this.runsByCompletionStatusCount.STATUS_RUNNING > 0) {
        code = 'running';
        text = `running (${this.runsByCompletionStatusCount.STATUS_RUNNING})`;
      }
    }
    return [code, text];
  }
};

const RUN_STATUSES = [
  'unknown',
  'succeeded',
  'failed',
  'running'
];

const state = {
  repository: {
    dataResources: {}
  },
  dataResources: [],
  dataResourcesTracked: {},
  currentDataResource: null,
  pageDataResources: 1,
  pageLengthDataResources: 15,
  sortDataResources: 'packageName',
  orderDataResources: 'asc',
  filtersDataResources: {},
  inProgressDataResources: {},
  pagesRequestedDataResources: [],
  pageCountDataResources: {},
  refreshRequestedDataResources: false,
  fileTypeFilters: {},
  organizations: {},
  resourceOrganizationFilters: [],
  fileTypeFiltersAllowed: [
    'CSV',
    'JSON',
    'GeoJSON'
  ],
  sourceFilters: {},
  summary: {},
  statusChangeReasons: {
    'Lintol\\Capstone\\Services\\Rules\\SizeMatch': 'File size does not match a processor\'s requirements',
    'Lintol\\Capstone\\Services\\Rules\\NameMatch': 'Name does not match a processor\'s requirements',
    'Lintol\\Capstone\\Services\\Rules\\FileType': 'File type does not match a processor\'s requirements'
  }
};

// getters
const getters = {
  organizationTitle: state => {
    return (organization) => (organization in state.organizations) ? state.organizations[organization].title : null;
  },
  statusChangeDetail: state => {
    return (resource) => {
      if (resource.lastStatusChange) {
        if (resource.lastStatusChange.new_status === 'unmatched' && resource.lastStatusChange.detail) {
          var reasons = [];
          for (var profileId in resource.lastStatusChange.detail) {
            resource.lastStatusChange.detail[profileId].forEach(function (rule) {
              var reason = state.statusChangeReasons[rule];
              if (!reason) {
                reason = '(unknown)';
              }
              reasons.push({ profileId: profileId, reason: reason });
            });
          }
          return reasons;
        }
      }
      return null;
    };
  },
  summaries: state => {
    var statuses = {
      'results': {
        'now': {
          'dataResources': {},
          'runs': {}
        }
      }
    };

    for (var which in state.summary.results) {
      var runStatuses = {};
      for (var status in state.summary.results[which].run_statuses) {
        var statusId = parseInt(status);
        if (!statusId) {
          statusId = 0;
        }
        runStatuses[RUN_STATUSES[statusId]] = state.summary.results[which].run_statuses[status];
      }

      statuses.results[which] = {
        'dataResources': state.summary.results[which].resource_statuses,
        'runs': runStatuses
      };
    }
    return statuses;
  },
  dataResourcePageCount: state => {
    var pages = 0;
    Object.values(state.pageCountDataResources).forEach(function (count) {
      pages += count;
    });
    return pages;
  },
  dataResources: state => {
    /* Newer approach: compatible with dataResources reactivity rules.
     * However, related objects may have issues when using this style. */
    var column = state.sortDataResources;
    var direction = (state.orderDataResources === 'asc' ? 1 : -1);
    var resources = state.dataResources.sort(function (a, b) {
      var x = a[column];
      var y = b[column];

      if (typeof x === 'string' || x instanceof String) {
        x = x.toLowerCase();
      }

      if (typeof y === 'string' || y instanceof String) {
        y = y.toLowerCase();
      }

      if (x < y) {
        return direction * -1;
      }
      if (x > y) {
        return direction;
      }
      return 0;
    });

    /* Add derived resources */
    return resources.map(resource => {
      Object.assign(resource, ResourceMixin);
      return resource;
    });
  },
  resourceOrganizationFilters: (state) => {
    return state.resourceOrganizationFilters;
  },
  fileTypeFilters: (state) => {
    return state.fileTypeFilters;
  },
  sourceFilters: (state) => {
    return state.sourceFilters;
  }
};

// actions
const actions = {
  [a.UPDATE_DATA_RESOURCE_STATUS] ({ commit, state }, statusChangeEvent) {
    const resourceId = statusChangeEvent.dataResourceId;
    var store = fromState(state);
    var dataResource = store.find('dataResources', resourceId);
    const trackingNumber = state.dataResourcesTracked[resourceId];

    if (dataResource && trackingNumber && (trackingNumber === true || trackingNumber < statusChangeEvent.dataResourceStatusChangeId)) {
      dataResource.status = statusChangeEvent.dataResourceNewStatus;
      commit(m.APPEND_DATA_RESOURCES, [dataResource]);
      commit(m.ADD_DATA_RESOURCE_TRACKED, { resourceId, trackingNumber });
    }
  },
  [a.RECORD_DATA_RESOURCE_TRACKED] ({ commit }, resourceId) {
    commit(m.ADD_DATA_RESOURCE_TRACKED, { resourceId });
  },
  [a.STORE_SETTING_PROFILE_ID_FOR_DATA_RESOURCES] ({ commit, state, dispatch }, { profileId, resources, artifacts }) {
    var dataResourceUrls = resources.map(function (resource) {
      return { 'id': resource.id, 'providerId': resource.providerId, 'url': resource.url, 'filetype': resource.filetype };
    });
    var settingMap = {
      dataProfileId: profileId,
      dataResourceUrls: dataResourceUrls
    };

    if (artifacts) {
      settingMap.artifacts = artifacts;
    }

    var url = apiPrefix + '/dataResources/settings';

    return axios.post(url, settingMap).then((response) => {
      resources.forEach((resource) => {
        bus.$emit('data-resource-used', resource.id);
      });

      commit(m.SET_DATA_RESOURCES_PAGE, 1);
      dispatch(a.LOAD_DATA_RESOURCES, { reset: true, page: 1 });
    }, error => {
      console.log('Couldnt get data resources for account.:' + error);
    });
  },
  [a.UPDATE_DATA_RESOURCES_FILTERS] ({ commit, state, dispatch }, filters) {
    if (filters !== state.filtersDataResources) {
      commit(m.SET_DATA_RESOURCES_FILTERS, filters);
      commit(m.SET_DATA_RESOURCES_PAGE, 1);
      return dispatch(a.LOAD_DATA_RESOURCES, { reset: true, page: 1 });
    }
  },
  [a.UPDATE_DATA_RESOURCES_PAGE] ({ state, commit, dispatch }, page) {
    if (page !== state.pageDataResources) {
      commit(m.SET_DATA_RESOURCES_PAGE, page);
      return dispatch(a.LOAD_DATA_RESOURCES, { reset: true, page: page });
    }
    return Promise.resolve(false);
  },
  [a.UPDATE_DATA_RESOURCES_ORDER] ({ state, commit, dispatch }, order) {
    if (order !== state.orderDataResources) {
      commit(m.SET_DATA_RESOURCES_PAGE, 1);
      commit(m.SET_DATA_RESOURCES_ORDER, order);
      return dispatch(a.LOAD_DATA_RESOURCES, { reset: true, page: 1 });
    }
    return Promise.resolve(false);
  },
  [a.UPDATE_DATA_RESOURCES_SORT] ({ state, commit, dispatch }, sort) {
    if (sort !== state.sortDataResources) {
      commit(m.SET_DATA_RESOURCES_PAGE, 1);
      commit(m.SET_DATA_RESOURCES_SORT, sort);
      return dispatch(a.LOAD_DATA_RESOURCES, { reset: true, page: 1 });
    }
    return Promise.resolve(false);
  },
  [a.LOAD_DATA_RESOURCE] ({ commit, state, dispatch }, { provider, resourceId, reload, includePackage, withRemoteMetadata }) {
    if (!reload && state.currentDataResource && state.currentDataResource.id === resourceId) {
      return;
    }

    let url = apiPrefix + '/dataResources/' + resourceId + '?provider=' + provider;

    if (includePackage) {
      url += '&include=package';
    }

    if (withRemoteMetadata) {
      url += '&fields[dataResources]=remoteMetadata';
    }

    axios.get(url).then((response) => {
      var resource = response.data;
      commit(m.APPEND_DATA_RESOURCES, { data: resource.data });
      commit(m.SET_CURRENT_DATA_RESOURCE, resource);
    }).catch((error) => {
      console.log('Couldnt get data resource for account:' + error);
    });
  },
  [a.LOAD_DATA_RESOURCES] ({ commit, state, dispatch }, { reset, page, ignoreProgress }) {
    var providers = ['_local', '_remote'];

    var filtersDataResources = Object.keys(state.filtersDataResources)
      .filter((key) => {
        console.debug('filtersDataResources:', state.filtersDataResources[key]);
        return state.filtersDataResources[key];
      }).map((key) => {
        return key + ':' + state.filtersDataResources[key];
      }).join(',');

    var query = 'page=' + page +
        '&count=' + state.pageLengthDataResources +
        '&filters=' + filtersDataResources +
        '&sortBy=' + state.sortDataResources +
        '&order=' + state.orderDataResources;

    if (state.filtersDataResources && state.filtersDataResources.source) {
      providers = [state.filtersDataResources.source];
    }

    var stat = state.inProgressDataResources['_'];
    if (stat === ACTION_PENDING) {
      commit(m.SET_DATA_RESOURCE_PROVIDER_IN_PROGRESS, ['_', ACTION_REQUESTED]);
      return Promise.resolve(true);
    } else if (stat === ACTION_REQUESTED) {
      return Promise.resolve(true);
      /* Wait until this completes */
    }

    var search = state.filtersDataResources.search;
    if (search) {
      query += '&search=' + search;
    }

    if (reset) {
      commit(m.SET_DATA_RESOURCES_REFRESH_REQUESTED);
    }

    /*
    commit(m.ADD_DATA_RESOURCE_PAGE_REQUEST, page);
    if (!skipInterpolation) {
      for (var i = 1; i < page; i++) {
        if (state.pagesRequestedDataResources.indexOf(i) === -1) {
          dispatch(a.LOAD_DATA_RESOURCES, { reset: false, page: i, skipInterpolation: true });
        }
      }
    }
    /*
    if (!skipInterpolation && state.inProgressDataResources[provider] === ACTION_PENDING) {
      console.log('requesting', provider);
      commit(m.SET_DATA_RESOURCE_PROVIDER_IN_PROGRESS, [provider, ACTION_REQUESTED]);
    } else if (!skipInterpolation && state.inProgressDataResources[provider] === ACTION_REQUESTED) {
      /* Already pending and requested *
    } else {
      */
    commit(m.SET_DATA_RESOURCE_PROVIDER_IN_PROGRESS, ['_', ACTION_PENDING]);

    return axios.get(apiPrefix + '/dataResources' + '?provider=' + providers.join(',') + '&' + query).then((response) => {
      var resources = response.data;
      /* While this approach works for stepping through,
       * further investigation may be required around jumping cold into page N>1 */
      if (state.refreshRequestedDataResources) {
        commit(m.RESET_DATA_RESOURCES);
      }
      commit(m.APPEND_DATA_RESOURCES, resources);

      var actionRequested = (state.inProgressDataResources['_'] === ACTION_REQUESTED);
      commit(m.SET_DATA_RESOURCE_PROVIDER_IN_PROGRESS, ['_', ACTION_NONE]);
      commit(m.SET_DATA_RESOURCE_PROVIDER_PAGE_COUNT, ['_', resources.meta.pagination.total_pages]);
      if (actionRequested) {
        return dispatch(a.LOAD_DATA_RESOURCES, { reset: true, page: state.pageDataResources });
      }

      return Promise.resolve(true);
    }).catch((error) => {
      console.log('Couldnt get data resources for account.:' + error);
      commit(m.SET_DATA_RESOURCE_PROVIDER_IN_PROGRESS, ['_', ACTION_NONE]);
      return Promise.resolve(false);
    });
    // }
  },
  [a.SAVE_DATA_RESOURCE] ({ commit }, resource) {
    var url = apiPrefix + '/dataResources/' + resource.id;
    var jsonDataResource = toModel('dataResources', resource);

    return axios.put(url, jsonDataResource).then((response) => {
      var resource = response.data;
      commit(m.SET_CURRENT_DATA_RESOURCE, resource);
    }).catch(function (error) {
      console.log('Error saving data resource:' + error);
    });
  },
  [a.DELETE_DATA_RESOURCE] ({ commit }, resource) {
    var url = apiPrefix + '/dataResources/' + resource.id;
    return axios.delete(url).then((response) => {
      var resource = response.data;
      commit(m.UNSET_CURRENT_DATA_RESOURCE, resource);
    }).catch(function (error) {
      console.log('Error deleting data resource:' + error);
    });
  },
  [a.STORE_DATA_RESOURCE] ({ commit }, dataResource) {
    var url = apiPrefix + '/dataResources';

    return axios.post(url, dataResource).then((response) => {
      console.log('Added data resource');
    }).catch(function (error) {
      console.log('Error adding data resource:' + error);
    });
  },
  [a.LOAD_DATA_RESOURCE_FILTER_SOURCE] ({ state, commit }) {
    var url = apiPrefix + '/dataResourcesFilters/' + 'getSourceFilters';
    return axios.get(url).then((response) => {
      var filters = response.data;
      commit(m.SET_DATA_RESOURCE_FILTER_SOURCE, filters);
    }).catch(function (error) {
      console.log('Error get data resource sources for filters:' + error);
    });
  },
  [a.LOAD_DATA_RESOURCE_SUMMARY] ({ state, commit, dispatch }, { from, to, when }) {
    var url = apiPrefix + '/dataResources/all/summary?from=' + from + '&to=' + to;
    if (when) {
      url += '&createdSince=' + when.toISOString();
    }
    return axios.get(url).then((response) => {
      var summary = response.data;
      commit(m.SET_DATA_RESOURCE_SUMMARY, summary);
    }).catch(function (error) {
      console.log('Error get data resource types for summary:' + error);
    });
  },
  [a.LOAD_DATA_RESOURCE_FILTER_ORGANIZATION] ({ state, commit, dispatch }) {
    var url = apiPrefix + '/dataResourcesFilters/' + 'getOrganizationFilters';
    return axios.get(url).then((response) => {
      var filters = response.data;
      commit(m.SET_DATA_RESOURCE_FILTER_ORGANIZATION, filters);
    }).catch(function (error) {
      console.log('Error get data resource organizations for filters:' + error);
    });
  },
  [a.LOAD_DATA_RESOURCE_FILTER_FILETYPE] ({ state, commit, dispatch }) {
    var url = apiPrefix + '/dataResourcesFilters/' + 'getFileTypeFilters';
    return axios.get(url).then((response) => {
      var filters = response.data;
      commit(m.SET_DATA_RESOURCE_FILTER_FILETYPE, filters);
    }).catch(function (error) {
      console.log('Error get data resource types for filters:' + error);
    });
  }
};

// mutations
const mutations = {
  [m.ADD_DATA_RESOURCE_TRACKED] (state, { resourceId, trackingNumber }) {
    if (!trackingNumber) {
      trackingNumber = true;
    }
    state.dataResourcesTracked[resourceId] = trackingNumber;
  },
  [m.SET_DATA_RESOURCE_FILTER_SOURCE] (state, sourceFilters) {
    state.sourceFilters = sourceFilters;
  },
  [m.SET_DATA_RESOURCE_FILTER_ORGANIZATION] (state, organizationFilters) {
    state.resourceOrganizationFilters = organizationFilters;
    state.organizations = organizationFilters.reduce(function (orgMap, org) {
      orgMap[org.name] = org;
      return orgMap;
    }, {});
  },
  [m.SET_DATA_RESOURCE_FILTER_FILETYPE] (state, fileTypeFilters) {
    state.fileTypeFilters = fileTypeFilters
      .filter((x) => (state.fileTypeFiltersAllowed.indexOf(x.filetype) !== -1))
      .reduce((o, v) => { o[v.filetype] = v; return o; }, {});
  },
  [m.SET_DATA_RESOURCES_FILTERS] (state, filters) {
    state.filtersDataResources = filters;
  },
  [m.SET_DATA_RESOURCE_SUMMARY] (state, summary) {
    state.summary = summary;
  },
  [m.SET_DATA_RESOURCES_PAGE] (state, page) {
    state.pageDataResources = page;
  },
  [m.SET_DATA_RESOURCES_SORT] (state, sort) {
    state.sortDataResources = sort;
  },
  [m.SET_DATA_RESOURCES_ORDER] (state, order) {
    state.orderDataResources = order;
  },
  [m.APPEND_DATA_RESOURCES] (state, dataResources) {
    var store = fromState(state);
    store.sync(dataResources);
    state.dataResources = store.findAll('dataResources');
  },
  [m.RESET_DATA_RESOURCES] (state) {
    state.repository.dataResources = {};
    state.refreshRequestedDataResources = false;
    state.dataResources = [];
  },
  [m.SET_CURRENT_DATA_RESOURCE] (state, dataResource) {
    var store = fromState(state);
    state.currentDataResource = store.find('dataResources', dataResource.data.id);
  },
  [m.UNSET_CURRENT_DATA_RESOURCE] (state) {
    state.currentDataResource = null;
  },
  [m.SET_DATA_RESOURCE_PROVIDER_IN_PROGRESS] (state, setting) {
    state.inProgressDataResources[setting[0]] = setting[1];
  },

  [m.SET_DATA_RESOURCE_PROVIDER_PAGE_COUNT] (state, setting) {
    Vue.set(state.pageCountDataResources, setting[0], setting[1]);
  },

  [m.ADD_DATA_RESOURCE_PAGE_REQUEST] (state, pageNumber) {
    state.pagesRequestedDataResources.push(pageNumber);
  },

  [m.SET_DATA_RESOURCES_REFRESH_REQUESTED] (state) {
    state.refreshRequestedDataResources = true;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
