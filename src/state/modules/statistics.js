import * as a from '../action-types';
import * as m from '../mutation-types';
import axios from 'axios';

var apiPrefix = process.env.VUE_APP_API_PREFIX;

const state = {
  entities: {}
};

// getters
const getters = {
};

// actions
const actions = {
  [a.LOAD_STATISTICS_ENTITIES] ({ commit }) {
    var url = apiPrefix + '/statistics/entities';

    return axios.get(url).then((response) => {
      var entities = response.data.results;
      commit(m.SET_STATISTICS_ENTITIES, entities);
    }, error => {
      console.log('Couldnt get statistics entities.:' + error);
    });
  }
};

// mutations
const mutations = {
  [m.SET_STATISTICS_ENTITIES] (state, entities) {
    state.entities = entities;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
