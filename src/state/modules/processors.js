import { fromState } from '../jsonapi';
import * as a from '../action-types';
import * as m from '../mutation-types';
import axios from 'axios';

var apiPrefix = process.env.VUE_APP_API_PREFIX;

const state = {
  repository: {
    processors: {}
  },
  processorActions: []
};

// getters
const getters = {
  processors: state => {
    return fromState(state).findAll('processors');
  },
  findProcessor: state => {
    return (id) => fromState(state).find('processors', id);
  }
};

// actions
const actions = {
  [a.RESTART_PROCESSOR] ({ commit }, { id }) {
    var url = apiPrefix + '/processors/' + id + '/actions';
    return axios.post(url, {
      action: 'restart'
    }).then((response) => {
      commit(m.ADD_PROCESSOR_ACTION, {
        action: 'restart',
        processorId: id
      });
    });
  },
  [a.LOAD_PROCESSORS] ({ commit }) {
    var url = apiPrefix + '/processors/';

    return axios(url).then((response) => {
      var processors = response.data;
      commit(m.SET_PROCESSORS, processors);
    }).catch(function (error) {
      console.log('Error get processors:' + error);
    });
  },
  [a.STORE_PROCESSOR] ({ commit }, processor) {
    var url = apiPrefix + '/processors';

    axios.post(url, processor).then((response) => {
      console.log('Added processor');
    }).catch(function (error) {
      console.log('Error adding processor:' + error);
    });
  }
};

// mutations
const mutations = {
  [m.SET_PROCESSORS] (state, processors) {
    state.repository.processors = {};
    fromState(state).sync(processors);
  },
  [m.RESET_PROCESSORS] (state) {
    state.repository.processors = {};
  },
  [m.SET_CURRENT_PROCESSOR] (state, processor) {
    var store = fromState(state);
    store.sync(processor);
    state.currentProcessor = store.find('processors', processor.data.id);
  },
  [m.UNSET_CURRENT_PROCESSOR] (state) {
    state.currentProcessor = null;
  },
  [m.ADD_PROCESSOR_ACTION] (state, action) {
    state.processorActions.push(action);
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
