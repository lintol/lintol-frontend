import { fromState, toModel } from '../jsonapi';
import * as a from '../action-types';
import * as m from '../mutation-types';
import axios from 'axios';

var apiPrefix = process.env.VUE_APP_API_PREFIX;

const state = {
  repository: {
    profiles: {}
  },
  currentProfile: {}
};

// getters
const getters = {
  profiles: state => {
    return fromState(state).findAll('profiles');
  },
  findProfile: state => {
    return (id) => fromState(state).find('profiles', id);
  },
  findProcessorConfiguration: state => {
    return (id) => fromState(state).find('processorConfigurations', id);
  }
};

// actions
const actions = {
  [a.LOAD_PROFILES] ({ commit }) {
    return axios.get(apiPrefix + '/profiles/').then((response) => {
      var profiles = response.data;
      commit(m.SET_PROFILES, profiles);
    }, error => {
      console.log('Couldnt get data profiles for account.:' + error);
    });
  },
  [a.LOAD_PROFILE] ({ commit }, profileId) {
    console.log(profileId, 'pid');
    if (state.currentProfile && state.currentProfile.id === profileId) {
      return Promise.resolve(state.currentProfile);
    }
    return axios.get(apiPrefix + '/profiles/' + profileId + '?include=configurations.processor').then((response) => {
      var profile = response.data;
      commit(m.SET_CURRENT_PROFILE, profile);
      return Promise.resolve(state.currentProfile);
    }, error => {
      console.log('Couldnt get data profiles for account.:' + error);
      return Promise.reject(error);
    });
  },
  [a.SAVE_PROFILE] ({ commit }, { profile, configurations }) {
    var url = apiPrefix + '/profiles/' + profile.id + '?include=configurations.processor';

    var jsonProfile = toModel('profiles', profile, {
      configurations: { type: 'processorConfigurations', relations: configurations }
    });

    return axios.put(url, jsonProfile).then((response) => {
      var profile = response.data;
      commit(m.SET_CURRENT_PROFILE, profile);
    }).catch(function (error) {
      console.log('Error adding profile:' + error);
    });
  },
  [a.STORE_PROFILE] ({ commit }, { profile, configurations }) {
    var jsonProfile = toModel('profiles', profile, {
      configurations: { type: 'processorConfigurations', relations: configurations }
    });
    var url = apiPrefix + '/profiles/?include=configurations.processor';

    return axios.post(url, jsonProfile).then((response) => {
      var profile = response.data;
      commit(m.SET_CURRENT_PROFILE, profile);
    }).catch(function (error) {
      console.log('Error adding profile:' + error);
    });
  }
};

// mutations
const mutations = {
  [m.SET_PROFILES] (state, profiles) {
    state.repository.profiles = {};
    fromState(state).sync(profiles);
  },
  [m.RESET_PROFILES] (state) {
    state.repository.profiles = {};
  },
  [m.SET_CURRENT_PROFILE] (state, profile) {
    var store = fromState(state);
    store.sync(profile);
    state.currentProfile = store.find('profiles', profile.data.id);
  },
  [m.UNSET_CURRENT_PROFILE] (state) {
    state.currentProfile = null;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
