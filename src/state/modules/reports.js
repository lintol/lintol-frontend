import { fromState } from '../jsonapi';
import * as a from '../action-types';
import * as m from '../mutation-types';
import axios from 'axios';

var apiPrefix = process.env.VUE_APP_API_PREFIX;
var REPORT_MAX_COUNT = 20;

const state = {
  repository: {
    reports: {},
    runs: {}
  },
  currentReport: null
};

// getters
const getters = {
  reports: state => {
    return fromState(state).findAll('reports');
  },
  incompleteRuns: state => {
    return fromState(state).findAll('runs').filter(function (run) {
      return run.incomplete === true;
    });
  }
};

// actions
const actions = {
  [a.LOAD_REPORTS] ({ commit }, { page, entity, id }) {
    var pageNum = page;
    if (!pageNum) {
      pageNum = 1;
    }

    var query = 'count=' + REPORT_MAX_COUNT + '&' +
      'page=' + pageNum;

    if (entity && id) {
      query += '&entity=' + entity + '&id=' + id;
    }

    var url = apiPrefix + '/reports/?' + query;

    return axios.get(url).then((response) => {
      var reports = response.data;
      commit(m.SET_REPORTS, reports);
    }, error => {
      console.log('Couldnt get reports.:' + error);
    });
  },
  [a.LOAD_INCOMPLETE_RUNS] ({ commit }) {
    var url = apiPrefix + '/runs/all/incomplete?include=dataResource,profile';

    return axios.get(url).then((response) => {
      var runs = response.data;
      commit(m.SET_INCOMPLETE_RUNS, runs);
    }, error => {
      console.log('Couldnt get incomplete runs.:' + error);
    });
  },
  [a.RETRIEVE_REPORT_ARTIFACT] ({ commit }, { reportId, tag, processor }) {
    var url = apiPrefix + '/reports/' + reportId + '/artifact?tag=' + tag + '&processor=' + processor;

    return axios.get(url).then((response) => {
      console.log(response);
    }, error => {
      console.log('Couldnt get artifact:' + error);
    });
  },
  [a.RERUN_REPORT] ({ commit }, { reportId, artifacts }) {
    var url = apiPrefix + '/reports/' + reportId + '/rerun';

    var data = {};

    if (artifacts) {
      data.artifacts = artifacts;
    }

    return axios.post(url, data).then((response) => {
      var runId = response.data;
      console.log('New run:' + runId);
    }, error => {
      console.log('Couldnt get report.:' + error);
    });
  },
  [a.DELETE_REPORT] ({ state, commit }, reportId) {
    var url = apiPrefix + '/reports/' + reportId;

    return axios.delete(url).then((response) => {
      var report = response.data;
      if (state.currentReport && report.data.id === state.currentReport.id) {
        commit(m.UNSET_CURRENT_REPORT);
      }
    }, error => {
      console.log('Couldnt delete report.:' + error);
    });
  },
  [a.LOAD_REPORT] ({ commit }, reportId) {
    var query = 'include=dataResource,profile,run';
    var url = apiPrefix + '/reports/' + reportId + '?' + query;
    commit(m.UNSET_CURRENT_REPORT);

    return axios.get(url).then((response) => {
      var report = response.data;
      commit(m.SET_CURRENT_REPORT, report);
    }, error => {
      console.log('Couldnt get report.:' + error);
    });
  }
};

// mutations
const mutations = {
  [m.SET_INCOMPLETE_RUNS] (state, incompleteRuns) {
    state.repository.runs = {};
    for (var run in state.repository.runs) {
      state.repository.runs[run].incomplete = undefined;
    }
    incompleteRuns.data.forEach(function (run) {
      run.attributes.incomplete = true;
    });
    console.log(incompleteRuns);
    fromState(state).sync(incompleteRuns);
  },
  [m.SET_REPORTS] (state, reports) {
    state.repository.reports = {};
    fromState(state).sync(reports);
  },
  [m.APPEND_REPORTS] (state, reports) {
    console.log(reports);
    fromState(state).sync(reports);
  },
  [m.RESET_REPORTS] (state) {
    state.repository.reports = {};
  },
  [m.SET_CURRENT_REPORT] (state, report) {
    var store = fromState(state);
    store.sync(report);
    state.currentReport = store.find('reports', report.data.id);
  },
  [m.UNSET_CURRENT_REPORT] (state) {
    state.currentReport = null;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
