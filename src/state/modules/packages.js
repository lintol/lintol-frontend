import { fromState } from '../jsonapi';
import * as a from '../action-types';
import * as m from '../mutation-types';
import axios from 'axios';

var apiPrefix = process.env.VUE_APP_API_PREFIX;

const state = {
  repository: {
    dataPackages: {}
  },
  dataPackages: [],
  currentDataPackages: null
};

// getters
const getters = {
};

// actions
const actions = {
  [a.LOAD_DATA_PACKAGE] ({ commit, state, dispatch }, { packageId, reload }) {
    if (!reload && state.currentDataPackage && state.currentDataPackage.id === packageId) {
      return;
    }
    axios.get(apiPrefix + '/dataPackages/' + packageId).then((response) => {
      var dataPackage = response.data;
      commit(m.APPEND_DATA_PACKAGES, { data: dataPackage.data });
      commit(m.SET_CURRENT_DATA_PACKAGE, dataPackage);
    }).catch((error) => {
      console.log('Couldnt get data package for account:' + error);
    });
  }
};

// mutations
const mutations = {
  [m.APPEND_DATA_PACKAGES] (state, dataPackages) {
    var store = fromState(state);
    store.sync(dataPackages);
    state.dataPackages = store.findAll('dataPackages');
  },
  [m.RESET_DATA_PACKAGES] (state) {
    state.repository.dataPackages = {};
    state.dataPackages = [];
  },
  [m.SET_CURRENT_DATA_PACKAGE] (state, dataPackage) {
    var store = fromState(state);
    state.currentDataPackages = store.find('dataPackages', dataPackage.data.id);
  },
  [m.UNSET_CURRENT_DATA_PACKAGE] (state) {
    state.currentDataPackage = null;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
