import Vuex from 'vuex';
import Vue from 'vue';
import axios from 'axios';
import users from './modules/users';
import processors from './modules/processors';
import packages from './modules/packages';
import profiles from './modules/profiles';
import reports from './modules/reports';
import resources from './modules/resources';
import statistics from './modules/statistics';

if (process.env.NODE_ENV !== 'testing') {
  var csrfToken = document.querySelector('meta[name="csrf-token"]').content;
  axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest',
    'X-CSRF-TOKEN': csrfToken
  };
  axios.interceptors.response.use(
    response => { return response; },
    function (error) {
      console.log(error);
      if (error.response.status === 401) {
        window.location = process.env.VUE_APP_DEV_LOGIN_URL;
      }
    }
  );
}
Vue.use(Vuex);

const store = new Vuex.Store({
  getters: {
    ready: function (state) {
      return state.users.loggedInUser !== null;
    }
  },
  modules: {
    users,
    packages,
    processors,
    profiles,
    reports,
    resources,
    statistics
  }
});

export default store;
