// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueFormGenerator from 'vue-form-generator';
import App from './App';
import router from './router';
import VeeValidate from 'vee-validate';
import store from './state/store';
// import * as Vue2Leaflet from 'vue2-leaflet'
import { LGeoJson, LTileLayer, LMarker, LPolygon, LMap } from 'vue2-leaflet';
import lodash from 'lodash';
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap';
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import VueWait from 'vue-wait';
import VueEcho from 'vue-echo-laravel';
import { bus, broadcast } from './event.js';

import Paginate from 'vuejs-paginate';
import vSelect from 'vue-select';

/* Work around to get Vue2Leaflet markers working */
import L from 'leaflet';
import { FieldArray } from 'vfg-field-array';

delete L.Icon.Default.prototype._getIconUrl;

window.io = require('socket.io-client');

L.Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});
/**************************************/

Vue.config.productionTip = false;
Vue.use(VeeValidate, { errorBagName: 'vErrors', fieldsBagName: 'vFields' });
Vue.use(VueFormGenerator);
Vue.use(BootstrapVue);
Vue.use(FieldArray);
Vue.use(VueEcho, {
  broadcaster: 'socket.io',
  host: process.env.VUE_APP_SOCKET_IO_ENDPOINT.replace('HOST', window.location.hostname)
});

// Vue.component('v-map', Vue2Leaflet.LMap)
// Vue.component('v-tilelayer', Vue2Leaflet.LTileLayer)
// Vue.component('v-marker', Vue2Leaflet.LMarker)
// Vue.component('v-polygon', Vue2Leaflet.LPolygon)
// Vue.component('v-geojson-layer', Vue2Leaflet.LGeoJSON)
Vue.component('v-map', LMap);
Vue.component('v-tilelayer', LTileLayer);
Vue.component('v-marker', LMarker);
Vue.component('v-polygon', LPolygon);
Vue.component('v-geojson-layer', LGeoJson);

Vue.component('paginate', Paginate);
Vue.component('v-select', vSelect);

Vue.prototype.dataPollingInterval = 60000;

Object.defineProperty(Vue.prototype, '$lodash', { value: lodash });

var features = [];
Object.defineProperty(Vue.prototype, '$bus', {
  get () {
    return this.$root.bus;
  }
});
Object.defineProperty(Vue.prototype, '$features', {
  get () {
    return this.$root.features;
  }
});

Vue.use(VueWait);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App),
  template: '<App/>',
  components: { App },
  store,
  data: {
    bus: bus,
    features: features
  },
  created () {
    broadcast.setStore(this.$store);
    broadcast.setEcho(this.$echo);
  },
  wait: new VueWait({
    useVuex: true,
    vuexModuleName: 'wait',
    registerComponent: true,
    componentName: 'v-wait'
  })
});

/* new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app') */
